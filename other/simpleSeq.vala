class SimpleSeq : GLib.Object { 

    public SimpleSeq add (int a){ result += a; return this;}
    public SimpleSeq sub (int a){ result -= a; return this;}
    public SimpleSeq mul (int a){ result *= a; return this;}
    public SimpleSeq div (int a){ result /= a; return this;}

    public int result = 0;

    public string to_string(){ return result.to_string();}
}

public static int main(string[] args) {
    var a = new SimpleSeq();
    
    a.add(5).mul(2).sub(2).div(4);
    print (@"$a");

    return 0;
}