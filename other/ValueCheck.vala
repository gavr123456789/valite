void main() {
    Value val = new Value(typeof(string));
    val = "sas";
    //  var a = val as (val.get_gtype()); //syntax error, expected `unowned'
    //  var a = (val.get_gtype)val;   //The symbol `val' could not be found
    var a = (string)val;          //works
    message (@"$(val.type_name())");

    val = 5;
    message (@"$(val.type_name())");
    val = false;    
    message (@"$(val.type_name())");
    val = 6.654;
    message (@"$(val.type_name())");
    var tableName = "5";

    var flag = (tableName == "")?true:false;
    (flag)? tableName = "da\n":tableName = "net\n";

    print(tableName);
    

    
    //  error("sas");
    //  print("asdasd");
}