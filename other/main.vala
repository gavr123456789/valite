using Gee;

void create_base_database (){
	var db = new DbController("create_test.db");
		var map = new HashMap<string,string>();
		map["sas"] = "TEXT";
		map["id"] = "NUMBER";
		map["date"] = "DATE";
		db.create(map);

		//int rand = Random.int_range(0,100000);
		db.insert( { @"$(quoting("a"))","1","'1/1/2000'"},
		{"sas","id", "date"},
		"User");

		db.insert( { @"$(quoting("b"))","2","'3/1/2000'"},
		{"sas","id", "date"},
		"User");

		db.insert( { @"$(quoting("c"))","3","'2/1/2000'"},
		{"sas","id", "date"},
		"User");
}
void tests( string[] args){
//Проверка на существование файла, удаление и создание нового
	var file = File.new_for_path("create_test.db");
	//если сужествует то удалить и создать
	if(file.query_exists())
		FileUtils.remove("create_test.db");	
	create_base_database();

	//  Test.add_func
	//  ("/db/create",() => {
	//  	assert (file.query_exists() == true);
	//  });


	Test.add_func
	("/db/insert",() => {
		var db = new DbController("create_test.db");	
		db.select_all("User");
		//db.print_pair();
		assert (db.pairArr[0].first == "id");
		assert (db.pairArr[0].second == "1");
	});
	Test.add_func
	("/db/select_from",() => {
		var db = new DbController("create_test.db");
		db.select_where("date","date LIKE '2%'");
		db.print_pair();
		assert (db.pairArr[0].first == "date");
		assert (db.pairArr[0].second == "2/1/2000");
	});	

	Test.add_func("/db/BUSY",() =>{
		assert (busy_test() == 0);
	});


	Test.init(ref args);
 	Test.run();
}
/////TEST BUSY START
int zx=0;
bool thread_func(){
	print(@"$(zx++)\n"); 
	var db = new DbController("create_test.db");	
	for (int a = 100; a < 201; a+=2) {
		db.insert( { @"$(quoting("a"))",@"$a","'1/1/2000'"},
		{"sas","id", "date"},
		"User");
	}
	
	return true;
}
bool thread_func2(){
	print(@"$(zx++)\n"); 
	var db = new DbController("create_test.db");	
	for (int a = 101; a < 201; a+=2) {
		db.insert( { @"$(quoting("a"))",@"$a","'1/1/2000'"},
		{"sas","id", "date"},
		"User");
	}
	
	return true;
}

int busy_test(){
	if(Thread.supported() == false) return -1;

	try{
		var t  = new Thread<bool>("test 1",thread_func);
		var t2 = new Thread<bool>("test 2",thread_func2);
		//  var t3 = new Thread<bool>("test 3",thread_func);

		bool z = t.join();
		bool z2 = t2.join();
		//  bool z3 = t3.join();
	} catch (Error e){
		critical("Error" + e.message); return -1;
	}
	return 0;
}
///TEST END

public static int main(string[] args) {

	//tests(args);
	
	parse_sharp();

	//  var map = new HashMap<string,string>();
	//  map["sas"] = "TEXT";
	//  map["id"] = "NUMBER";
	//  map["date"] = "DATE";

	//var db = new DbController("test.db");
	//db.create(map);

	//  int rand = Random.int_range(0,100000);
	//  int rand_data = Random.int_range(1980,2019);
	//  int rand_day = Random.int_range(1,30);


	//  db.insert( { @"$(quoting("a"))",@"$rand",@"'$rand_day/10/$rand_data'"},
	//  {"sas","id", "date"},
	//  "User");

	//print("DEBUG SELECT ALL\n");\
	// db.select_all("User");
	//print("DEBUG SELECT\n");
	//db.select("date","User");
	//  print("DEBUG SELECT_WHERE\n");
	//  db.select_where("date","date LIKE '2%'");
	//  print("OUTPUT: ");
	//  db.print_pair();
	//  print("DEBUG UPDATE\n");
	//    db.update("sas", "'a'", {@"$(quoting("b"))",@"$rand",@"'$rand_day/10/$rand_data'"},
	//    {"sas","id", "date"},
	//    "User");
	//print("DEBUG DELETE\n");
	//db.delete("sas", "'a'", "User");

	return 0;
}