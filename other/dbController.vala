using Gee;

string quoting (string str){
	return "'" + str + "'";
}
errordomain DbError { OPEN }

class DbController : Object { 

	public struct Pair {
		public string first;
		public string second;
	}

	Sqlite.Database db;
	string errmsg;

	public Pair[] pairArr;

	public void print_pair(){
		foreach (var a in pairArr) {
			print(@"$(a.first) : $(a.second)\n");
		}
	}
	
	public DbController(string name) throws DbError {
		// Open a database:
		int ec = Sqlite.Database.open (name, out db);
		if (ec != Sqlite.OK) {
			errmsg = @"Can't open database: $(db.errcode()) $(db.errmsg())\n";
			throw new DbError.OPEN(errmsg);
		}
	}

	//создание загаловка таблицы название к типу
	public int create (HashMap<string,string> map) {
		//set WAL mode
		int ec = db.exec ("pragma journal_mode = WAL", null, out errmsg);message("pragma journal_mode = WAL");
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}

		string query = "\nCREATE TABLE User (\n";
		
		var it = map.map_iterator();
		it.next();
		query += "\t" + it.get_key() + "\t" + it.get_value() + "\tPRIMARY KEY\tNOT NULL" +",\n";

		for (var has_next = it.next(); has_next; has_next = it.next() ) {
			query += "\t" + it.get_key() + "\t" + it.get_value() +"\tNOT NULL,\n";
		}

		string final_query = query[0:-2] + "\n);";
		print("result: " + final_query);

		ec = db.exec (final_query, null, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}

		print ("Created.\n");

		return 0;
	}

	//////////// map - значение к колонке
	public int insert (string[] row, string[] colums , string table_name) {

		//  //set WAL mode
		//  int ec = db.exec ("pragma journal_mode = WAL", null, out errmsg);message("SASSSSSSSSSSSSSSSSS2");
		//  if (ec != Sqlite.OK) {
		//  	stderr.printf ("Error: %s\n", errmsg);
		//  	return -1;
		//  }
		
		string query = "\n";
		string row_table    = string.joinv(", ", row);
		string column_table = string.joinv(", ", colums);

		row_table = "(" + row_table + ")"; 
		column_table = "(" + column_table + ")";
		assert (row.length == colums.length);
		query += "INSERT INTO " + table_name + " " + column_table + " VALUES " + row_table + ";\n";

		int ec = db.exec (query, null, out errmsg);

		// wait
		while(ec == 5) {
			Thread.usleep(100000);
			ec = db.exec (query, null, out errmsg);
		}

		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}

		int64 last_id = db.last_insert_rowid ();
		print (@"Last inserted id: $last_id $(row[1])\n");

		return 0;
	}

	public int insert_only_row (string[] row, string table_name) {
		
		string query = "\n";
		string row_table = string.joinv(", ", row);
	
		row_table = "(" + row_table + ")"; 
		query += "INSERT INTO " + table_name + " VALUES " + row_table + ";\n";

		int ec = db.exec (query, null, out errmsg);

		// wait
		while(ec == 5) {
			Thread.usleep(100000);
			ec = db.exec (query, null, out errmsg);
		}

		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}

		int64 last_id = db.last_insert_rowid ();
		print (@"Last inserted id: $last_id $(row[1])\n");

		return 0;
	}

	//////////////SELECT
	private int exec_callback (int n_columns, string[] values, string[] column_names) {
		string[,] arr = new string[column_names.length,values.length];  
		for (int i = 0; i < n_columns; i++) {
			//print ("%s: %s\n", column_names[i], values[i]);
			// заполнение результата
			Pair p = {column_names[i],values[i]}; 
			pairArr += p;
		}

		return 0;
	}
	public int select_all (string table_name) {
		string query = @"SELECT * FROM $table_name";
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}
		return 0;
	}

	public int select (string query2,string table_name) {
		string query = @"SELECT $query2 FROM $table_name";
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}
		return 0;
	}

	public int select_where (string query2, string where, string table_name = "User") {
		string query = @"SELECT $query2 FROM $table_name WHERE $where";
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}
		return 0;
	}
	/////////////////
	// FIXME: aaa
	public int update (string column, string value, string[] row, string[] colums, string table_name) {
		string query;
		//name = 'Hermann' WHERE id = 1;";
		for(int i = 0; i < row.length; i++) {
			query = "UPDATE " + table_name + " SET " + colums[i] + " = " + row[i] + " " + "WHERE " + column + " = " + value + ";";
			int ec = db.exec (query, null, out errmsg);
			if (ec != Sqlite.OK) {
				stderr.printf ("Error: %s\n", errmsg);
				return -1;
			}
		}
		int changes = db.total_changes ();
		print ("Affected rows: %d\n", changes);

		return 0;
	}
	/////////////
	public int delete (string column, string value, string table_name) {
		string query = "DELETE FROM " + table_name + " WHERE " + column + " = " + value + ";";
		int ec = db.exec (query, null, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}

		int changes = db.total_changes ();
		print ("Affected rows: %d\n", changes);

		return 0;
	}

}