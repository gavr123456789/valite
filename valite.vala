using Gee;
string quoting2 (string str){return "'" + str + "'";}
struct Current {
    string column;
    string table;
    string query;
}
class Valite : Object { 
    Sqlite.Database db;
	//ArrayList<string> arrayList;
	HashMap<string,string> map;
	string[] data = {};
	int dbStatus;
    string errmsg;
    bool is_a_map;
    int atomic_exec_status;
    Current current;
    
    public Valite(string dbname = "") {
        (dbname == "") ?
                dbStatus = Sqlite.Database.open (":memory:", out db):
                dbStatus = Sqlite.Database.open (dbname, out db);
        status_check();
        current.table = "tempTable";
        atomic_exec_status = 0;
        is_a_map = false;
		//map = new HashMap<string,string>();
    }



	void status_check(){
		if (dbStatus != Sqlite.OK) {
            stderr.printf ("Error: $(db.errcode ()): $(db.errmsg ())\n");
	    }
    }

	public int select_where (string query2, string where, string table_name = "User") {
        string query = @"SELECT $query2 FROM $table_name WHERE $where";
        AtomicInt.inc(ref atomic_exec_status);
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			return -1;
		}
		return 0;
    }

    void set_table(string table_name) {current.table = table_name;}
    
    string get(string column, string row, string table_name = "tempTable"){
        string query = @"SELECT $column\n FROM $table_name\n WHERE $row\n";
        AtomicInt.inc(ref atomic_exec_status);
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) 
			stderr.printf ("Error: %s\n", errmsg);
        
        while (atomic_exec_status != 0) 
            prin("waiting");
        
        var strBuilder = new StringBuilder();
        foreach (var item in toArrayList()) {
            strBuilder.append(item);
            strBuilder.append(" ");
        }
        return strBuilder.str;
    }
//TODO: Переделать data на Value везде, заюзав модули из BetterPrint
    void set(string column, string row, string data){
        //  string query = @"INSERT INTO $(current.table) $column";
        //  "INSERT INTO User (id, name) VALUES (1, 'Hesse');";
        string query = @"UPDATE $(current.table) SET $column = $data WHERE $row = 17;"; // TODO: Доделать
        AtomicInt.inc(ref atomic_exec_status);
		int ec = db.exec (query, exec_callback, out errmsg);
		if (ec != Sqlite.OK) {
			stderr.printf ("Error: %s\n", errmsg);
			
        }
        
        while (atomic_exec_status != 0) {
            prin("waiting");
        }

        var strBuilder = new StringBuilder();
        foreach (var item in toArrayList()) {
            strBuilder.append(item);
            strBuilder.append(" ");
        }
        return strBuilder.str;
      
    }



    public void exec(string query){
        AtomicInt.inc(ref atomic_exec_status);
		dbStatus = db.exec (query, null, out errmsg);
		status_check();	
    }
	//вторым аргументом функцию десереализации даты, шоб можно было пихать что угодно
    public void add(Value data, string tableName=""){
        var query = "";
        var typeName = data.type_name(); //message(typeName);
        string val = "";
		switch (typeName) {
            case "gint":      { val = @"$((int)data)";       break;}
            case "gchararray":{ val = quoting2((string)data); break;}
            case "gboolean":  { val = @"$((bool)data)";      break;}
            case "gdouble":   { val = @"$((double)data)";    break;}
            default: { val = to_json_string(data); message(to_json_string(data));  break; /*error("unknown type");*/} /*error завершает прогу сам */ break;//TODO: когда будет добавлена функция сравнения учесть это здесь чтоб не выкидывало error	
            //TODO: помойму можно сделать чтобы вылетала error с помощью boxed_can_deserialize в typeof(тип пришедшего из стринг)
        }
        var tableNameFinal = "";
        //если tablename не равен "" и существует то добавить туда, если не сущ то добавить столбец 
        tableNameFinal = (tableName != "")? tableName: "tempTable";
        //query += (typeName == "gint")? @"INSERT INTO  (name, hint) VALUES ('year', 3021);"
        
        query = @"CREATE TABLE IF NOT EXISTS $tableNameFinal (temp);\n" + 
                @"INSERT INTO $tableNameFinal VALUES ($val)";

        //message("query to add:\t" + query);
        exec(query);
        status_check();
    }

    private string to_json_string(Value obj){
        //message(@"$(Json.gobject_to_data(obj.get_object(),null))");
        return Json.gobject_to_data(obj.get_object(),null);
        //message(json_data);
    }
    
    private int exec_callback (int n_columns, string[] values, string[] column_names) {
        AtomicInt.dec_and_test(ref atomic_exec_status);
        for (int i = 0; i < n_columns; i++) {
			//print ("%s: %s\n", column_names[i], values[i]);
			data+=values[i];
			//map[column_names[i]] = values[i]; 
			//message(@"$(map.size)");
        }
        return 0;
    }
    
    public Valite select_all(string tableName = ""){
        var query = "SELECT * FROM ";
        query += (tableName=="")? "tempTable":@"$tableName";
        AtomicInt.inc(ref atomic_exec_status);
        dbStatus = db.exec (query, exec_callback, out errmsg);
        status_check();
        return this;
    }

    public Valite order()  {return this;}
    public Valite limit(int lim)  {return this;}
    public Valite group()  {return this;}
    public Valite having() {return this;}
    public Valite where()  {return this;}

    public ArrayList<string> toArrayList(){
		var arrayList = new ArrayList<string>();
		foreach (var a in data) 
			arrayList.add(a);
		return arrayList;
	}
	public HashSet<string> toSet(){
		var myset = new HashSet<string> ();
		foreach (var a in data) 
			myset.add(a);
		return myset;
    }

}


public static int main(string[] args) {
    Test.init(ref args);

    Test.add_func("/Valite/create_db_in_memory", ()=> {
        var fname = "db.db";
        var db = new Valite(fname);
        assert (FileUtils.test (fname, FileTest.EXISTS));
        FileUtils.remove(fname);	

    });
    //TODO переделать лол, это не проверка
    //  Test.add_func("/Valite/create_db_in_local_storage", ()=> {
    //      var fname = "db.db";
    //      var db = new Valite(fname);
    //      assert (FileUtils.test (fname, FileTest.EXISTS));
    //  });
    //TODO: добавить проверку что значение добавилось в таблицу
    //  Test.add_func("/Valite/add_value_to_db", ()=> {
    //      var fname = "db.db";
    //      var db = new Valite();
    //      db.add(5);
    //  });
    Test.add_func("/Valite/add_Object_to_db", ()=> {
        var db = new Valite();
        db.add(new TestClass("test", 5, true));
        foreach (var z in db.select_all().toArrayList()) prin(@"$z");
    });
    Test.add_func("/Valite/select_all", ()=> {
        var db = new Valite();
        db.add(5);
        db.add("asd");
        //db.select_all();
	});
	Test.add_func("/Valite/toArrayList", ()=> {
        var db = new Valite();
        db.add(5);
		db.add("asd");
		var rar = db.select_all().toArrayList();//.@foreach((g) => {prin(@"$g");return false;});
		foreach (var z in rar) prin(@"$z");
			
	});
	
	Test.add_func("/Valite/toSet", ()=> {
        var db = new Valite();
        db.add(5);db.add(5);
		assert (db.select_all().toArrayList().size == 2);
		assert (db.select_all().toSet().size       == 1);
    });

    Test.add_func("/Valite/get", ()=> {
        var db = new Valite();
        db.add(5);db.add(5);
        
		//assert (db.select_all().toSet().size == 1);
    });
    
    Test.run();

    return 0;
}