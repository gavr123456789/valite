# Valite

High-level sqlite3 wrapper for Vala with Seq support and implementing some GUI interfaces to work with Sqlite as with a usual collection (there is a mode of storing the database in memory).
## Example
```vala
var db = new Valite();
db.add(5);
db.add("text");
db.select_all();
```
#### Output
```
temp: 5
```

## TODO

- [ ] add from GTK/GLib [ListStore](https://valadoc.org/gtk+-3.0/Gtk.ListStore.html) 
- [ ] add from GTK/GLib [TreeStore](https://valadoc.org/gtk+-3.0/Gtk.TreeStore.html)
- [ ] add from Gee [ArrayList](https://valadoc.org/gee-0.8/Gee.ArrayList.html)
- [x] add func select_all
- [x] add func add that can take any basic type data as argument
- [ ] in operator : `(5 in db)?`
- [ ] add iterator: `foreach (var a in db)`
- [ ] add set syntax sugar : `db["table/column name"] = 5`
- [ ] add get syntax sugar : `("text" in db["column name"])?`
- [ ] linq like sequences 
```vala
db.from("table name, or maybe collection")
  .where((a)=>{a.has_suffix("a")})
  .select("*");
and other SQL key words for SELECT: GROUP BY, ORDER BY, LIMIT, JOIN, HAVING
```